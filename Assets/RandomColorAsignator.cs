﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomColorAsignator : MonoBehaviour
{
    public Color[] colors;
    public Color color;
    // Start is called before the first frame update
    void Start()
    {
        color = colors[Random.Range(0, colors.Length-1)];
        this.GetComponent<MeshRenderer>().material.color = color;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
