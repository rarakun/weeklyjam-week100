﻿using System;
using UnityEngine;

public interface IDeliveryCar
{
    string Id { get; set; }
    void CancelCurrent();
    void SetDeliveryTask(DeliveryTask currentDeliveryTask);
    void SetColor(Color color);
    void SetModel(CarModel model);
    void ShowIndicator(bool visible);
    Transform transform { get; }
    bool IsHuman { get; }
    TaskManager.CarStatusInfo Status { get; set; }

    DeliveryTask CurrentTask { get; }
}