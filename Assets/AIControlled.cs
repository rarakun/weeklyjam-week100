﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AIControlled : AICar, IDeliveryCar
{
    public float horizontalSpeed = 2f;
    public float forwardSpeed = 2f;
    public string Id { get; set; }

    public bool IsHuman => false;

    public Car car;
    public DeliveryTask CurrentTask { get; private set; } = new DeliveryTask();
    public TaskManager.CarStatusInfo Status { get; set; }

    protected bool TaskCompleted;
    protected void Awake()
    {
        car = this.GetComponent<Car>();
    }
    protected override void OnEnable()
    {
        base.OnEnable();
    }

    protected override void AssignNextRandomTask()
    {
        TaskCompleted = false;
        if (TaskManager.Instance.Ended)
        {
            Stop();
        }

    }

    // Update is called once per frame
    protected  override void Update()
    {
        agent.isStopped = !Status.CanMove;
        if (CurrentTask == null) AssignNextRandomTask();
        Move();
        if (IsTaskComplete())
        {
            AssignNextRandomTask();
        } 

    }
    public void DeliveryStarted(Spot spot)
    {
    }
    public void DeliveryStopped(Spot spot)
    {
    }
    public void DeliveryCompleted()
    {
        TaskCompleted = true;
    }
    public void CancelCurrent()
    {
        TaskCompleted = true;
    }
    public override bool IsTaskComplete()
    {
        return TaskCompleted;
    }
    public void SetDeliveryTask(DeliveryTask task)
    {
        TaskCompleted = false;
        CurrentTask = task;
        currentTask = CurrentTask.Target;
        SetDestination();
    }

    public void SetColor(Color color)
    {
        this.car.SetColor(color);
    }

    public void ShowIndicator(bool visible)
    {
        this.car.ShowIndicator(visible);
    }

    public void SetModel(CarModel model)
    {
        car.Model = model;
    }
}