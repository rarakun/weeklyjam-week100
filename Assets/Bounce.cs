﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bounce : MonoBehaviour
{
    public float BounceMax = 1;
    // Start is called before the first frame update
    void Start()
    {

        transform.DOPunchPosition(transform.up, 5f);
    }

    // Update is called once per frame
    void Update()
    {
    }
}
