﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TrackTaskTimeLeft : MonoBehaviour
{
    public UnityEngine.UI.Text Text;

    public string TargetId;
    // Start is called before the first frame update
    void OnEnable()
    {
        Text = this.GetComponent<UnityEngine.UI.Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (TaskManager.Instance.CarStatus.ContainsKey(TargetId))
        {
            var status = TaskManager.Instance.CarStatus[TargetId];
            Text.text = ((int)(status.CurrentTask.ExpectedTime - status.CurrentTask.ElapsedTime)).ToString();
        } else
        {
            Text.text = "?";
        }
    }
}
