﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrackStatus : MonoBehaviour
{
    public string TargetId;
    public GameObject ScorePrefab;
    public Transform ScoreContainer;
    private TrackTaskProgress Progress;
    private TrackTaskTimeLeft TimeLeft;
    public Text NameLabel;
    // Start is called before the first frame update
    void OnEnable()
    {
        Progress = GetComponentInChildren<TrackTaskProgress>();
        TimeLeft = GetComponentInChildren<TrackTaskTimeLeft>();
    }


    // Update is called once per frame
    void Update()
    {
        var status = TaskManager.Instance.CarStatus[TargetId];
        if (Progress.TargetId != TargetId)
            Progress.TargetId = TargetId;
        if (TimeLeft.TargetId != TargetId)
            TimeLeft.TargetId = TargetId;
        NameLabel.text = status.Name;
        NameLabel.color = status.color;
        var diff = status.TotalDeliveries - (ScoreContainer.childCount - 1);
        if (diff>0)
        {
            for (int i = 0; i < diff; i++)
            {
                var score = GameObject.Instantiate(ScorePrefab, ScoreContainer);
            }
            Progress.transform.SetAsLastSibling();
        }
        if (TaskManager.Instance.Ended)
            ScoreContainer.GetChild(ScoreContainer.childCount-1).gameObject.SetActive(false);
    }
}
