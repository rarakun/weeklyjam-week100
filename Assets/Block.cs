﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    public Spot[] Spots;
    public int MaxBuildings = 3;
    public float Size = 15;
    public Material BuildingMaterial;
    // Start is called before the first frame update
    void Start()
    {
        MaxBuildings = Random.Range(3, 12);
        var buildingSize = Size / MaxBuildings;
        var center = Size / 2;
        for (var x = 0; x < MaxBuildings; x++)
        {
            for (var y = 0; y < MaxBuildings; y++)
            {
                CreateBuilding(new Vector3(x * buildingSize -center + buildingSize/2, 0, y * buildingSize - center + buildingSize / 2), $"Building ${x}:${y}", buildingSize);
            }
        }
    }

    private void CreateBuilding(Vector3 position, string name, float size)
    {
        var buildingPos = new GameObject(name);
        buildingPos.transform.parent = transform;
        buildingPos.transform.position = transform.position + position;
        var building = GameObject.CreatePrimitive(PrimitiveType.Cube);
        building.GetComponent<BoxCollider>().enabled = false;
        building.transform.parent = buildingPos.transform;
        building.transform.localScale = new Vector3(size, Random.Range(1, 15), size);
        building.transform.localPosition = new Vector3(0, building.transform.localScale.y / 2, 0);
        building.GetComponent<MeshRenderer>().material = BuildingMaterial;
    }
}
