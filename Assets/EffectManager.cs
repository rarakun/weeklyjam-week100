﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectManager : MonoBehaviour
{
    public Effect[] effects;
    public static EffectManager Instance { get; private set; }
    public GameObject SmokePrefab;
    public GameObject SparksPrefab;
    private Dictionary<string, Effect> loadedEffects = new Dictionary<string, Effect>();
    public EffectManager()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        if (effects != null)
        {
            for (int i = 0; i < effects.Length; i++)
            {
                loadedEffects.Add(effects[i].Id, effects[i]);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void PlayEffect(string effectName, Vector3 position, Quaternion rotation, Transform parent = null)
    {
        if (loadedEffects.ContainsKey(effectName))
        {
            var GO = SimplePool.Spawn(loadedEffects[effectName].Prefab, position, rotation);
            GO.transform.SetParent(parent);
        }
        else
        {
            Debug.Log("PlayEffect: " + effectName);
        }
    }
}
[System.Serializable]
public struct Effect
{
    public string Id;
    public GameObject Prefab;
}
