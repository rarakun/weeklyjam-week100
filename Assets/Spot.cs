﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spot : MonoBehaviour
{
    public Color color;
    public Color currentColor;
    public MeshRenderer[] renderers;
    void Start()
    {
        if (renderers.Length > 0 )
        {
            color = renderers[0].material.GetColor("_EmissionColor");
        }
        ResetStatus();
    }

    // Update is called once per frame
    void Update()
    {
    }
    public void ResetStatus()
    {
        SetRenderersColor(color);
        currentColor = color;
    }


    public void SetRenderersColor(Color targetColor)
    {
        targetColor.a = color.a;
        for (int i = 0; i < renderers.Length; i++)
        {
            renderers[i].material.SetColor("_EmissionColor", targetColor);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        IDeliveryCar controlled = other.GetComponent<IDeliveryCar>();
        if (controlled != null)
        {
            TaskManager.Instance.CarArrived(this, controlled);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        IDeliveryCar controlled = other.GetComponent<IDeliveryCar>();
        if (controlled != null)
        {
            TaskManager.Instance.CarLeft(this, controlled);
        }
    }
}
