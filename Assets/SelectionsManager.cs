﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionsManager : MonoBehaviour
{
    public GameObject SpotPrefab;
    public GameObject CardPrefab;
    public Transform CardContainer;

    public List<PlayerStatus> Statuses = new List<PlayerStatus>();

    // Start is called before the first frame update
    void Start()
    {
        CheckStatuses();
    }

    // Update is called once per frame
    void Update()
    {
    }

    [ContextMenu("Set Statuses")]
    private void CheckStatuses()
    {
        for (int i = 0; i < Statuses.Count; i++)
        {
            PlayerStatus status = Statuses[i];

            if (status.Spot == null)
            {
                var spot = GameObject.Instantiate(SpotPrefab, transform.position + new Vector3(Statuses.Count * -50, 0, 0), Quaternion.identity, transform);
                var spotController = spot.GetComponent<SpotController>();
                status.Spot = spotController;
                spotController.PlayerStatus = status;
                spotController.Load();
            }
            if (status.Card == null)
            {
                var card = GameObject.Instantiate(CardPrefab, CardContainer);
                var cardController = card.GetComponent<CardController>();
                status.Card = cardController;
                cardController.PlayerStatus = status;
                cardController.Load();
            }
        }
    }
}
[System.Serializable]
public struct PlayerStatus
{
    public RenderTexture PreviewTexture;
    public string Name;
    public string Status;

    internal SpotController Spot;
    internal CardController Card;
}
