﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleHandler : MonoBehaviour
{
    public string LevelName;
    public string MenuLevelName;
    public GameObject QuitButton;
    public void Quit()
    {
        Application.Quit();
    }
    public void Play()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(LevelName, LoadSceneMode.Single);
    }
    public void BackToMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(MenuLevelName, LoadSceneMode.Single);
    }
#if UNITY_WEBGL	
    public void OnEnable()
    {
        if (QuitButton!=null)
            QuitButton.SetActive(false);
    }
#endif
}
