﻿using UnityEngine;

public class Car : MonoBehaviour
{
    public Renderer[] Models;
    private Color color;
    public GameObject Indicator;
    public CarModel Model;
    public Vector3 ExhaustOffset;
    public void SetColor(Color color)
    {
        if (Models != null && Models.Length > 0)
        {
            for (int i = 0; i < Models.Length; i++)
            {
                Models[i].material.color = color;
            }
        }
        for (int i = 0; i < Model.materials.Length; i++)
        {
            Model.meshRenderer.materials[Model.materials[i]].color = color;
        }
    }
    public void ShowIndicator(bool visible)
    {
        Indicator.SetActive(visible);
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, transform.position + transform.forward * 4);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.CompareTag("Ground"))
        {
            var contact = collision.GetContact(0);
            EffectManager.Instance.PlayEffect("Collision", contact.point, Quaternion.LookRotation(contact.thisCollider.bounds.center - contact.point));

        }
    }
    private void Start()
    {
        EffectManager.Instance.PlayEffect("PermanentSmoke", transform.position + ExhaustOffset,Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y +180, transform.rotation.eulerAngles.z), transform);
    }
}
