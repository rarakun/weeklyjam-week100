﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackTaskProgress : MonoBehaviour
{
    public UnityEngine.UI.Image Image;

    public string TargetId;
    public TaskManager.CarStatusInfo Status;
    // Start is called before the first frame update
    void Start()
    {
        Image = this.GetComponent<UnityEngine.UI.Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (TaskManager.Instance.CarStatus.ContainsKey(TargetId))
        {
            Status = TaskManager.Instance.CarStatus[TargetId];
            if (Status.CurrentTask != default(DeliveryTask))
                Image.fillAmount = 1 - Status.CurrentTask.ElapsedTime / Status.CurrentTask.ExpectedTime;
        }
    }
}
