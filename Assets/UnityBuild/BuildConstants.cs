// This file is auto-generated. Do not modify or move this file.

public static class BuildConstants
{
    public enum ReleaseType
    {
        None,
        JamRelease,
        Dev,
    }

    public enum Platform
    {
        None,
        PC,
        WebGL,
    }

    public enum Architecture
    {
        None,
        Windows_x86,
        Windows_x64,
        WebGL,
    }

    public enum Distribution
    {
        None,
        Itchio,
    }

    public static readonly System.DateTime buildDate = new System.DateTime(636976277014037951);
    public const string version = "1.0.1643.313";
    public const ReleaseType releaseType = ReleaseType.Dev;
    public const Platform platform = Platform.WebGL;
    public const Architecture architecture = Architecture.WebGL;
    public const Distribution distribution = Distribution.Itchio;
}

