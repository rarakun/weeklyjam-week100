﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DespawnAfterTime : MonoBehaviour
{
    public float Lifetime = 10;
    private float Elapsed;

    private void OnEnable()
    {
        Elapsed = 0;
    }
    // Update is called once per frame
    void Update()
    {
        Elapsed += Time.deltaTime;
        if (Elapsed >= Lifetime) SimplePool.Despawn(gameObject);
    }
}
