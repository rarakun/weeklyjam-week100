﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotController : MonoBehaviour
{
    public PlayerStatus PlayerStatus;

    private UnityEngine.Camera CameraComponent;
    private Transform Pivot;
    private Transform Base;
    // Start is called before the first frame update
    void Awake()
    {
        Init();
    }
    void OnEnable()
    {
        Load();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Init()
    {
        CameraComponent = CameraComponent ?? transform.Find("Spot Camera").GetComponent<Camera>();
        Pivot = Pivot ?? transform.Find("Pivot");
        Base = Base ?? Pivot.Find("Base");
    }
    public void Load()
    {
        Init();
        CameraComponent.targetTexture = PlayerStatus.PreviewTexture;
    }
}
