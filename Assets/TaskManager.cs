﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TaskManager : MonoBehaviour
{
    public static TaskManager Instance { get; private set; }
    private List<GameObject> Cars = new List<GameObject>();
    public List<IDeliveryCar> DeliveryCars = new List<IDeliveryCar>();
    public GameObject CarPrefab;
    public Transform CarContainer;
    public Follow CameraTracker;
    public Dictionary<string, CarStatusInfo> CarStatus = new Dictionary<string, CarStatusInfo>();
    public UnityEngine.Events.UnityEvent Victory;
    public UnityEngine.Events.UnityEvent Failure;
    public Color[] CarColors;
    public Transform UIScoreContainer;
    public GameObject ScorePrefab;
    public GameObject[] CarModels;
    private List<Color> AvailableColors = new List<Color>();
    public List<PlayerInfo> Players = new List<PlayerInfo>();


    public int WinningAmountDeliveries = 10;
    public int TargetCarCount = 100;

    public GameObject PizzaPrefab;
    public bool Started;
    public bool Ended;
    [Serializable]
    public class CarStatusInfo
    {
        public int Score;
        public int TotalDeliveries;
        public string Id;
        public string Name;
        public Color color;
        public bool CanMove;
        public bool IsHuman;
        public DeliveryTask CurrentTask;
    }

    public float ElapsedTime;
    public NewTaskManager TaskManagerInstance;
    public LevelManager LevelManager;
    public TaskManager()
    {
        Instance = this;
        TaskManagerInstance = new NewTaskManager();
        TaskManagerInstance.OnTaskCompleted += TaskManagerInstance_OnTaskCompleted;
        TaskManagerInstance.OnTaskFailed += TaskManagerInstance_OnTaskFailed;
        LevelManager = new LevelManager();
    }

    private void TaskManagerInstance_OnTaskFailed(DeliveryTask task)
    {
        for (int i = 0; i < DeliveryCars.Count; i++)
        {
            DeliveryCars[i].CancelCurrent();
            AddScore(DeliveryCars[i], task.Penalty);
        }
        if (!Ended) CreateTaskAndAssign();
    }

    private void TaskManagerInstance_OnTaskCompleted(DeliveryTask task)
    {
        AddScore(task.CurrentCar, task.Reward);
        if (!Ended) CreateTaskAndAssign();
    }

    // Start is called before the first frame update
    void Awake()
    {
    }

    internal void CarArrived(Spot spot, IDeliveryCar car)
    {
        for (int i = 0; i < TaskManagerInstance.ActiveTasks.Count; i++)
        {
            if (TaskManagerInstance.ActiveTasks[i].Target == spot)
            {
                TaskManagerInstance.ActiveTasks[i].AddCar(car);
            }
        }
    }
    internal void CarLeft(Spot spot, IDeliveryCar car)
    {
        for (int i = 0; i < TaskManagerInstance.ActiveTasks.Count; i++)
        {
            if (TaskManagerInstance.ActiveTasks[i].Target == spot)
            {
                TaskManagerInstance.ActiveTasks[i].RemoveCar(car);
            }
        }
    }

    public void AddScore(IDeliveryCar car, int points)
    {
        CarStatus[car.Id].Score += points;
        CarStatus[car.Id].TotalDeliveries += 1;
        if (CarStatus[car.Id].TotalDeliveries >= WinningAmountDeliveries)
        {
            ShowResults(CarStatus[car.Id]);
        }

    }

    private void ShowResults(CarStatusInfo info)
    {
        Ended = true;
        TaskManagerInstance.ClearTasks();
        if (info.IsHuman && info.TotalDeliveries >= WinningAmountDeliveries)
        {
            Victory.Invoke();
        }
        else
        {
            Failure.Invoke();
        }
    }

    void Start()
    {
        AvailableColors.AddRange(CarColors);
        LoadPlayersInfo();
        LevelManager.LoadLevel();
        LoadNPCs();
        LoadPlayersCars();
        StartCoroutine(StartGame());
    }


    IEnumerator StartGame()
    {
        yield return new WaitForSeconds(8);
        Started = true;
        for (int i = 0; i < DeliveryCars.Count; i++)
        {
            DeliveryCars[i].Status.CanMove = true;
        }
    }

    private void LoadPlayersInfo()
    {
        Players.Add(new PlayerInfo() { Id = "01", Name = "Human", IsAI = false, Color = PullColor() });
        Players.Add(new PlayerInfo() { Id = "02", Name = "AI 1", IsAI = true, Color = PullColor() });
        Players.Add(new PlayerInfo() { Id = "03", Name = "AI 2", IsAI = true, Color = PullColor() });
        Players.Add(new PlayerInfo() { Id = "04", Name = "AI 3", IsAI = true, Color = PullColor() });
    }
    private void LoadNPCs()
    {
        InstanceCars();
    }
    private void LoadPlayersCars()
    {
        for (int i = 0; i < Players.Count; i++)
        {
            var info = Players[i];
            Func<string, Color, Spot, IDeliveryCar> creator = null;
            if (info.IsAI)
            {
                creator = InstanceNewAICar;
            }
            else
            {
                creator = InstanceNewPlayerCar; ;
            }
            IDeliveryCar deliveryCar = InstanceControlledCar(creator, info.Id, info.Name, info.Color);
        }
        CreateTaskAndAssign();
    }

    private void CreateTaskAndAssign()
    {
        var task = TaskManagerInstance.CreateDeliveryTask(GameObject.Instantiate(PizzaPrefab), TaskManager.Instance.LevelManager.GetRandomSpot());
        for (int i = 0; i < DeliveryCars.Count; i++)
        {
            DeliveryCars[i].SetDeliveryTask(task);
            CarStatus[DeliveryCars[i].Id].CurrentTask = task;
        }
    }

    private IDeliveryCar InstanceControlledCar(Func<string, Color, Spot, IDeliveryCar> instantiate, string id, string name, Color color)
    {
        var spot = LevelManager.GetRandomSpot();
        IDeliveryCar deliveryCar = instantiate(id, color, spot);
        deliveryCar.Status = new CarStatusInfo()
        {
            Id = deliveryCar.Id,
            Name = name,
            IsHuman = deliveryCar.IsHuman,
            color = color,
            Score = 0,
            CanMove = false,
        };
        CarStatus.Add(deliveryCar.Id,deliveryCar.Status);
        var TrackStatus = GameObject.Instantiate(ScorePrefab, UIScoreContainer).GetComponent<TrackStatus>();
        TrackStatus.TargetId = deliveryCar.Id;
        Debug.Log(string.Join(",", CarStatus.Keys.ToArray()));
        return deliveryCar;
    }

    private Color PullColor()
    {
        Color color = AvailableColors[UnityEngine.Random.Range(0, AvailableColors.Count)];
        AvailableColors.Remove(color);
        return color;
    }

    private bool instancing = false;
    public void InstanceCars()
    {
        if (instancing) return;
        instancing = true;
        for (int i = Cars.Count; i < TargetCarCount; i++)
        {
            Color color = AvailableColors[UnityEngine.Random.Range(0, AvailableColors.Count)];
            var spot = LevelManager.GetRandomSpot();
            var newCar = GameObject.Instantiate(CarPrefab, spot.transform.position, Quaternion.identity, CarContainer);
            var model = GameObject.Instantiate(CarModels[UnityEngine.Random.Range(0, CarModels.Length)], newCar.transform.position, Quaternion.Euler(0, 180, 0), newCar.transform).GetComponent<CarModel>();
            var car = newCar.GetComponent<Car>();
            car.Model = model;
            car.SetColor(color);
            newCar.AddComponent<AICar>();
            Cars.Add(newCar);
        }
        instancing = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Started)
        {
            ElapsedTime += Time.deltaTime;
        }
        if (!instancing && Cars.Count < TargetCarCount) InstanceCars();
        TaskManagerInstance.Update();
    }

    private IDeliveryCar InstanceNewPlayerCar(string id, Color color, Spot spot)
    {
        var newCar = GameObject.Instantiate(CarPrefab, spot.transform.position, Quaternion.identity, CarContainer);
        var model = GameObject.Instantiate(CarModels[UnityEngine.Random.Range(0, CarModels.Length)], newCar.transform.position, Quaternion.Euler(0, 180, 0), newCar.transform).GetComponent<CarModel>();
        newCar.GetComponent<AudioSource>().volume = 0.1f;
        PlayerControlled deliveryCar = newCar.AddComponent<PlayerControlled>();
        deliveryCar.SetModel(model);
        deliveryCar.SetColor(color);
        deliveryCar.ShowIndicator(true);
        deliveryCar.Id = id.ToString();
        newCar.name = deliveryCar.Id;
        DeliveryCars.Add(deliveryCar);
        CameraTracker.target = deliveryCar.transform;
        return deliveryCar;
    }

    private IDeliveryCar InstanceNewAICar(string id, Color color, Spot spot)
    {
        var newCar = GameObject.Instantiate(CarPrefab, spot.transform.position, Quaternion.identity, CarContainer);
        var model = GameObject.Instantiate(CarModels[UnityEngine.Random.Range(0, CarModels.Length)], newCar.transform.position, Quaternion.Euler(0, 180, 0), newCar.transform).GetComponent<CarModel>();
        AIControlled deliveryCar = newCar.AddComponent<AIControlled>();
        deliveryCar.SetModel(model);
        deliveryCar.SetColor(color);
        deliveryCar.ShowIndicator(true);
        deliveryCar.Id = id;
        deliveryCar.GetComponent<UnityEngine.AI.NavMeshAgent>().speed = UnityEngine.Random.Range(5, 8);
        newCar.name = deliveryCar.Id;
        DeliveryCars.Add(deliveryCar);
        return deliveryCar;
    }
    public struct PlayerInfo
    {
        public string Id;
        public string Name;
        public Color Color;
        public bool IsAI;
    }
}
public class NewTaskManager
{
    public static NewTaskManager Instance { get; private set; }
    public List<DeliveryTask> ActiveTasks = new List<DeliveryTask>();
    public GameObject IndicatorPrefab;
    public event DeliveryTask.TaskHandler OnTaskCompleted;
    public event DeliveryTask.TaskHandler OnTaskFailed;

    public NewTaskManager()
    {
        Instance = this;
    }
    private void ActivateTask(DeliveryTask task)
    {
        task.Indicator.transform.position = task.Target.transform.position + Vector3.up * 3;
        task.Target.gameObject.SetActive(true);
        ActiveTasks.Add(task);
    }
    void UpdateTask(DeliveryTask task)
    {
        if (task.CurrentCar == null)
        {
            if (!task.Indicator.gameObject.activeInHierarchy)
                task.Indicator.gameObject.SetActive(false);
        }
        else if (task.CurrentCar != null)
        {
            if (task.Progress == 0f)
            {
                if (task.Indicator.gameObject.activeInHierarchy)
                    task.Indicator.gameObject.SetActive(true);
            }
            task.Progress += Time.deltaTime;
        }
        task.ElapsedTime += Time.deltaTime;
        if (task.Expired())
        {
            CancelTask(task);
        }
        else if (task.Completed())
        {
            task.Indicator.gameObject.SetActive(false);
            Debug.Log("Completed!", task.Target);
            task.Target.gameObject.SetActive(false);
            EffectManager.Instance.PlayEffect("DeliveryCompleted", task.Target.transform.position, Quaternion.identity);
            OnTaskCompleted.Invoke(task);
            ActiveTasks.Remove(task);
        }
    }
    public void CancelTask(DeliveryTask task)
    {
        task.Indicator.gameObject.SetActive(false);
        task.Target.gameObject.SetActive(false);
        OnTaskFailed.Invoke(task);
        ActiveTasks.Remove(task);
    }
    void TaskCurrentChanged(DeliveryTask task)
    {
        Debug.Log("Current Task CHanged");
        if (task.CurrentCar == null)
        {
            task.Target.ResetStatus();
        }
        else if (task.CurrentCar != null)
        {
            task.Target.SetRenderersColor(task.CurrentCar.Status.color);
        }
    }

    internal DeliveryTask CreateDeliveryTask(GameObject indicatorInstance, Spot target)
    {
        var task = new DeliveryTask()
        {
            Target = target,
            ExpectedTime = 30f,
            Reward = 5,
        };
        task.Indicator = indicatorInstance;
        task.Indicator.gameObject.SetActive(false);
        task.CurrentChanged += TaskCurrentChanged;
        ActivateTask(task);
        return task;
    }

    internal void ClearTasks()
    {
        ActiveTasks.Clear();
    }
    public void Update()
    {
        for (int i = 0; i < ActiveTasks.Count; i++)
        {
            UpdateTask(ActiveTasks[i]);
        }
    }
}
public class LevelManager
{
    public Block[] Blocks;

    public void LoadLevel()
    {
        Blocks = GameObject.FindObjectsOfType<Block>();
    }
    public Spot GetRandomSpot()
    {
        if (Blocks == null) return null;

        var targetBlock = UnityEngine.Random.Range(0, Blocks.Length);
        int spotIndex = UnityEngine.Random.Range(0, Blocks[targetBlock].Spots.Length);
        var targetSpot = Blocks[targetBlock].Spots[spotIndex];
        return targetSpot;
    }
}
