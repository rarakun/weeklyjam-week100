﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class PlayerControlled : MonoBehaviour, IDeliveryCar
{
    Rigidbody body;
    public float horizontalSpeed = 100f;
    public float forwardSpeed = 1f;
    public string Id { get; set; }

    public bool IsHuman => true;

    public DeliveryTask CurrentTask { get; private set; }
    public Car car;
    public string HorizontalAxis = "Horizontal";
    public string VerticalAxis = "Vertical";
    public float Volume = 0.2f;
    public TaskManager.CarStatusInfo Status { get; set; }

    private AudioSource audioSource;

    void Awake()
    {
        car = this.GetComponent<Car>();
    }
    // Start is called before the first frame update
    void Start()
    {
        body = this.GetComponent<Rigidbody>();
        audioSource = this.GetComponent<AudioSource>();
        audioSource.volume = Volume;

    }

    // Update is called once per frame
    void Update()
    {
        audioSource.volume = Volume;
        if (!Status.CanMove) return;
        var horizontal = Input.GetAxis(HorizontalAxis);
        var vertical = Input.GetAxis(VerticalAxis);
        this.body.AddTorque(new Vector3(0, horizontal * horizontalSpeed, 0));
        this.body.velocity += transform.forward * vertical * forwardSpeed;

        Debug.DrawLine(transform.position, transform.position + body.velocity.normalized * 3, Color.yellow);
        Debug.DrawLine(transform.position, transform.position + body.angularVelocity.normalized * 2, Color.blue);
        //this.body.angularVelocity += transform.forward * horizontal * horizontalSpeed;
    }
    public void DeliveryStarted(Spot spot)
    {
    }
    public void DeliveryStopped(Spot spot)
    {
    }
    public void DeliveryCompleted()
    {
    }
    public void CancelCurrent()
    {
    }

    public void SetDeliveryTask(DeliveryTask currentDeliveryTask)
    {
        this.CurrentTask = currentDeliveryTask;
    }

    public void SetColor(Color color)
    {
        car.SetColor(color);
    }

    public void ShowIndicator(bool visible)
    {
        car.ShowIndicator(visible);
    }

    public void SetModel(CarModel model)
    {
        car.Model = model;
    }
}
