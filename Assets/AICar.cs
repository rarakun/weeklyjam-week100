﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AICar : MonoBehaviour
{
    protected Spot currentTask;
    public NavMeshAgent agent;
    // Start is called before the first frame update
    protected virtual void OnEnable()
    {
        agent = this.GetComponent<NavMeshAgent>();
        AssignNextRandomTask();
    }

    protected virtual void AssignNextRandomTask()
    {
        currentTask = TaskManager.Instance.LevelManager.GetRandomSpot();
        if (currentTask!=null )
            SetDestination();
    }

    protected void SetDestination()
    {
        if (currentTask!=null)
            agent.SetDestination(currentTask.transform.position);
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        Move();
        if (IsTaskComplete())
        {
            AssignNextRandomTask();
        }
    }

    public virtual bool IsTaskComplete()
    {
        return currentTask == null || agent.remainingDistance != Mathf.Infinity && agent.pathStatus == NavMeshPathStatus.PathComplete && agent.remainingDistance == 0;
    }
    protected void Move()
    {
        agent.Move(Vector3.zero);
    }
    protected void Stop()
    {
        agent.isStopped = true;
    }
    protected void Resume()
    {
        agent.isStopped = false;
    }
}
