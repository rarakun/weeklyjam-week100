﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DeliveryTask {
    public Spot Target;

    public float ExpectedTime;
    public float ExtraTime;
    public float ElapsedTime;


    public float Progress = 0;
    public float WaitTime = 5f;
    public IDeliveryCar CurrentCar { get; private set; }
    public List<IDeliveryCar> CarQueue = new List<IDeliveryCar>();
    public int Reward;
    internal int Penalty = -2;

    public delegate void TaskHandler(DeliveryTask task);
    public event TaskHandler CurrentChanged;
    public GameObject Indicator;

    internal void AddCar(IDeliveryCar car)
    {
        if (CurrentCar != null || CarQueue.Count > 0)
        {
            if (!CarQueue.Contains(car))
                CarQueue.Add(car);
        }
        else
        {
            SetCurrentCar(car);
        }
    }

    private void SetCurrentCar(IDeliveryCar car)
    {
        Progress = 0;
        CurrentCar = car;
        if (CurrentChanged != null) CurrentChanged.Invoke(this);
    }

    internal void RemoveCar(IDeliveryCar car)
    {
        if (CurrentCar == car)
        {
            RemoveCurrentCar();
        }
        else if (CarQueue.Contains(car))
        {
            CarQueue.Remove(car);
        }
    }

    private void RemoveCurrentCar()
    {
        Progress = 0;
        IDeliveryCar next = null;
        if (CarQueue.Count > 1)
        {
            next = CarQueue[0];
            CarQueue.RemoveAt(0);
        }
        SetCurrentCar(next);
    }

    public bool Expired()
    {
        return (ElapsedTime + ExtraTime) >= ExpectedTime;
    }
    internal bool Completed()
    {
        return Progress >= WaitTime;
    }
}