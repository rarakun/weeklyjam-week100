﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardController : MonoBehaviour
{
    public PlayerStatus PlayerStatus;


    private UnityEngine.UI.RawImage ImageComponent;
    private UnityEngine.UI.Text NameComponent;
    private UnityEngine.UI.Text StatusComponent;
    private void Awake()
    {
        Init();
    }
    // Start is called before the first frame update
    void OnEnable()
    {
        Load();
    }

    public void Init()
    {
        ImageComponent = ImageComponent ?? this.transform.Find("RawImage").GetComponent<RawImage>();
        NameComponent = NameComponent ?? this.transform.Find("Name").GetComponent<Text>();
        StatusComponent = StatusComponent ?? this.transform.Find("Status").GetComponent<Text>();
    }
    public void Load()
    {
        Init();
        ImageComponent.texture = PlayerStatus.PreviewTexture;
        NameComponent.text = PlayerStatus.Name;
        StatusComponent.text = PlayerStatus.Status;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
